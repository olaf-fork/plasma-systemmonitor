# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-systemmonitor package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-systemmonitor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-14 00:58+0000\n"
"PO-Revision-Date: 2021-12-09 18:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:49
#, kde-format
msgctxt "@title:window"
msgid "Details"
msgstr "Detaljinformation"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:73
#, kde-format
msgctxt "@title:group"
msgid "CPU"
msgstr "Processor"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:125
#, kde-format
msgctxt "@title:group"
msgid "Memory"
msgstr "Minne"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:151
#, kde-format
msgctxt "@title:group"
msgid "Network"
msgstr "Nätverk"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:176
#, kde-format
msgctxt "@title:group"
msgid "Disk"
msgstr "Disk"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:202
#, kde-format
msgctxt "@title:group"
msgid "Processes: %1"
msgstr "Processer: %1"

#: src/faces/applicationstable/contents/ui/ApplicationDetails.qml:263
#, kde-format
msgctxt "@info:placeholder"
msgid "Select an application to see its details"
msgstr "Markera ett program för att se dess information"

#: src/faces/applicationstable/contents/ui/ApplicationsTableView.qml:28
#, kde-format
msgctxt "Warning message shown on runtime error"
msgid "Applications view is unsupported on your system"
msgstr "Programvisning stöds  inte på systemet"

#: src/faces/applicationstable/contents/ui/Config.qml:24
#, kde-format
msgctxt "@option:check"
msgid "Show details panel"
msgstr "Visa informationspanel"

#: src/faces/applicationstable/contents/ui/Config.qml:29
#, kde-format
msgctxt "@option:check"
msgid "Confirm when quitting applications"
msgstr "Bekräfta när program avslutas"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:29
#, kde-format
msgctxt "@action"
msgid "Search"
msgstr "Sök"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:39
#, kde-format
msgctxt "@action"
msgid "Quit Application"
msgstr "Avsluta programmet"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:49
#, kde-format
msgctxt "@action"
msgid "Show Details Sidebar"
msgstr "Visa informationssidorad"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:57
#, kde-format
msgctxt "@action"
msgid "Configure columns…"
msgstr "Anpassa kolumner…"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:185
#, kde-format
msgctxt "@action:inmenu"
msgid "Set priority…"
msgstr "Ställ in prioritet…"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:188
#, kde-format
msgctxt "@action:inmenu"
msgid "Send Signal"
msgstr "Skicka signal"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:191
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Suspend (STOP)"
msgstr "Stoppa aktivitet (STOP)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:195
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Continue (CONT)"
msgstr "Återuppta aktivitet (CONT)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:199
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Hangup (HUP)"
msgstr "Lägg på (HUP)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:203
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Interrupt (INT)"
msgstr "Avbryt aktivitet (INT)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:207
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Terminate (TERM)"
msgstr "Avsluta aktivitet (TERM)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:211
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Kill (KILL)"
msgstr "Döda aktivitet (KILL)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:215
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 1 (USR1)"
msgstr "Användarsignal 1 (USR1)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:219
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 2 (USR2)"
msgstr "Användarsignal 2 (USR2)"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:226
#, kde-format
msgctxt "@action:inmenu"
msgid "Quit Application"
msgid_plural "Quit %1 Applications"
msgstr[0] "Avsluta programmet"
msgstr[1] "Avsluta %1 program"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:244
#, kde-format
msgctxt "@title:window"
msgid "Quit Application"
msgid_plural "Quit %1 Applications"
msgstr[0] "Avsluta programmet"
msgstr[1] "Avsluta %1 program"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:245
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Quit Application"
msgctxt "@action:button"
msgid "Quit Application"
msgid_plural "Quit Applications"
msgstr[0] "Avsluta programmet"
msgstr[1] "Avsluta programmet"

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:247
#, kde-format
msgid ""
"Are you sure you want to quit this application?\n"
"Any unsaved work may be lost."
msgid_plural ""
"Are you sure you want to quit these %1 applications?\n"
"Any unsaved work may be lost."
msgstr[0] ""
"Är du säker på att du vill avsluta det här programmet?\n"
"Allt osparat arbete kan gå förlorat."
msgstr[1] ""
"Är du säker på att du vill avsluta de här %1 programmen?\n"
"Allt osparat arbete kan gå förlorat."

#: src/faces/applicationstable/contents/ui/FullRepresentation.qml:256
#, kde-format
msgctxt "@item:intable"
msgid "%1 Process"
msgid_plural "%1 Processes"
msgstr[0] "Process"
msgstr[1] "%1 processer"

#~ msgctxt "@action:button"
#~ msgid "Quit"
#~ msgstr "Avsluta"
